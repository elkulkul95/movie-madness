import { Component, OnInit } from '@angular/core';
import { WordService } from '../shared/word.service';
import { Protag } from '../shared/protag.model';

@Component({
  selector: 'app-movie-generator',
  templateUrl: './movie-generator.component.html',
  styleUrls: ['./movie-generator.component.css']
})
export class MovieGeneratorComponent implements OnInit {

  public word: string;
  public protags: Protag[];

  constructor(private words: WordService) { }

  ngOnInit() {
    this.generate();
  }

  generate() {
    this.word = this.words.get('color');

    this.protags = [
      this.words.getProtag(),
      this.words.getProtag(),
      this.words.getProtag(),
      this.words.getProtag(),
      this.words.getProtag(),
      this.words.getProtag(),
      this.words.getProtag(),
      this.words.getProtag(),
      this.words.getProtag(),
      this.words.getProtag(),
    ];
  }

}
